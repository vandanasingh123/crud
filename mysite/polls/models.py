from django.db import models

# Create your models here.
from django.db import models

class CrudUser(models.Model):
    name = models.CharField(max_length=30, blank=True)
    pan_number = models.TextField(max_length=100, blank=True,null=True)
    age = models.IntegerField(blank=True, null=True)
    gender =models.TextField(max_length=100, blank=True)
    email =models.EmailField(blank=True)
    city = models.TextField(max_length=100, blank=True)
    def __str__(self):
        return self.name