from django.shortcuts import render

# Create your views here.

from django.views.generic import ListView
from .models import CrudUser
from django.views.generic import View
from django.http import JsonResponse

class CrudView(ListView):
    model = CrudUser
    template_name = 'crud.html'
    context_object_name = 'users'


class CreateCrudUser(View):
    def  get(self, request):
        name1 = request.GET.get('name', None)
        pan1 = request.GET.get('pan_number', None)
        age1 = request.GET.get('age', None)
        gender1 = request.GET.get('gender', None)
        email1 = request.GET.get('email', None)
        city1 = request.GET.get('city', None)

        obj = CrudUser.objects.create(
            name = name1,
            pan_number = pan1,
            age = age1,
            gender = gender1,
            email=email1,
            city=city1,
        )

        user = {'id':obj.id,'name':obj.name,'pan_number':obj.pan_number,'age':obj.age,'gender':obj.gender,'email':obj.email,'city':obj.city}

        data = {
            'user': user
        }
        return JsonResponse(data)



class UpdateCrudUser(View):
    def  get(self, request):
        id1 = request.GET.get('id', None)
        name1 = request.GET.get('name', None)
        pan1 = request.GET.get('pan_number', None)
        age1 = request.GET.get('age', None)
        gender1 = request.GET.get('gender', None)
        email1 = request.GET.get('email', None)
        city1 = request.GET.get('city', None)

        obj = CrudUser.objects.get(id=id1)
        obj.name = name1
        obj.pan_number = pan1
        obj.age = age1
        obj.gender = gender1
        obj.email = email1
        obj.city = city1
        obj.save()

        user = {'id':obj.id,'name':obj.name,'pan_number':obj.pan_number,'age':obj.age,'gender':obj.gender,'email':obj.email,'city':obj.city}

        data = {
            'user': user
        }
        return JsonResponse(data)

class DeleteCrudUser(View):
        def get(self, request):
            id1 = request.GET.get('id', None)
            CrudUser.objects.get(id=id1).delete()
            data = {
                'deleted': True
            }
            return JsonResponse(data)
